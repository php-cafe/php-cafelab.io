---
tags:
- standar
title: "[PHP-3] - What the heck is a PSR"
date: 2019-05-17T00:00:00+02:00

---
Love and estandards in PHP souse

> PSR stands for PHP Standard Recommendation.

## Basics

[PSR](https://www.php-fig.org/psr/ "PSRs") are recommendations mainteined by the [PHP-FIG](https://www.php-fig.org/ "PHP_FIG") (PHP Framework Interoperability Group). The aim is create a common grount for every PHP developer so that the re-utilization of code and competency is allowed and simpler.

Each PSR is identified by a number, PSR-0, PSR-1 PSR-4 are all different PSR. The important thing to bear in mind is that the numbering DO NOT imply that one PSR is "more", "less" or even "related to" the previews or the following one. You can find related ones, like PSR-0 and PSR-4 (Autoloading Standard) or PSR-1, PSR-2 and PSR-12 (Coding Style Guide) but nothing more.

## Why do I have to care?

You "have to" care, know and follow the PSRs because they allow you to be much more productive and let you understand better code that is not your own. Libraries, bundles or even entire framework or product respect some, if not all, of them.

### Ok, which is the more important to know?

In my experience I can say without doubt that the "more important" PSRs are the following:

* [PSR-2](https://www.php-fig.org/psr/psr-2/ "PSR-2"): Coding Style
* [PSR-4](https://www.php-fig.org/psr/psr-4/ "PSR-4"): Autoloader
* [PSR-18](https://www.php-fig.org/psr/psr-18/ "PSR-18"): HTTP Client
* [PSR-16](https://www.php-fig.org/psr/psr-16/ "PSR-16"): Common Interface for Caching Libraries
* [PSR-11](https://www.php-fig.org/psr/psr-11/ "PSR-11"): Container interface