---
title: "[PHP-1] - Composer"
date: 2019-04-29T18:00:00.000+00:00
tags:
- code

---
A basic component of every new PHP project, tiny or huge, will be [composer](https://getcomposer.org/) As the project define itself:

> Composer is a tool for dependency management in PHP.It allows you to declare the libraries your project depends on and it will manage (install/update) them for you.

## Basics

### Installation

There are a couple of ways to install composer, [locally](https://getcomposer.org/doc/00-intro.md#locally "Install composer locally") or [globally](https://getcomposer.org/doc/00-intro.md#globally "Install composer globally") but I suggest to use the global one everywhere is possible. In the global installation the syntax is shorter and, you can find yourself interacting with composer lots of times a day, especially in the beginning and for project maintenance. Or simply use docker as shown in the following section example.

### Use

One installed we can summon composer directly by _composer_ or via docker

    docker run --rm --interactive --tty \
      --volume $PWD:/app \
      composer

We can use composer from the real beginning of a project, because it can be used to create an empty one or use a blueprint.

Let's cut to the chase and get us a new project

    mkdir myproject
    cd myproject
    docker run --rm --interactive --tty \
      --volume $PWD:/app \
      composer init

This will ask some questions and setup a basic file: composer.json

    {
        "name": "root/app",
        "require": {}
    }

this is an empty composer.json

## How to add dependencies

### Require

Let assume we're going to start well this project of ours. We're going to need some tests.  
How do we add [PHPUnit](https://phpunit.de/ "PHPUnit") for example?

    docker run --rm --interactive --tty \
     --volume $PWD:/app \
     --volume $COMPOSER_HOME:/tmp \
     composer require --dev phpunit/phpunit

Wow, there is a lot of new thing here!

* _--volume $COMPOSER_HOME:/tmp_ use a temporal directory for cache
* _composer require_ command to add a dependency
* _--dev_ add the dependency to the dev part (it will not be included in a production environment)
* _phpunit/phpunit_ where the hell does this comes from?

Well the [_phpunit/phpunit_](https://packagist.org/packages/phpunit/phpunit "phpunit/phpunit") comes from [packagist](https://packagist.org/ "Packagist") that is the composer "companion". Packagist is a registry of every library that can be installed with composer.

> Packagist is the default Composer package repository. It lets you find packages and lets Composer know where to get the code from. You can use Composer to manage your project or libraries' dependencies - read more about it on the [Composer website](https://getcomposer.org/).

### Vendor and composer.lock

The last command _composer require --dev phpunit/phpunit_ left us with some new thing in the project directory:

* composer.lock file
* vendor directory

The composer.lock file is part of the "magic" of composer. It allow to track the "real" version of everything that we have installed in our local version and replicate the exact same situation everywhere we need. No guessing games or questions like "What version are you using of the library x?". Everything is consistent.

Now... the vendor directory. This is a good moment for adding this directory to the gitignore file. Unless you know what you're doing DO NOT track this directory.

Another fundamental part of what we have here is this file: _vendor/autoload.php_  
The [autoload](https://getcomposer.org/doc/01-basic-usage.md#autoloading "autoloading") is the process by witch we're going to have full access of every PHP code in the vendor directory without includes.

> For libraries that specify autoload information, Composer generates a `vendor/autoload.php` file. You can simply include this file and start using the classes that those libraries provide without any extra work

More details en the next chapter [PSR-4](/post/php-2-psr-4/ "PSR-4").