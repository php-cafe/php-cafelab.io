---
title: "[PHP-2] - PSR-4"
date: 2019-05-13T00:00:00+02:00
tags:
- code

---
[PSR-4](https://www.php-fig.org/psr/psr-4/ "PSR-4") is an important part of a modern PHP project

> This PSR describes a specification for [autoloading](http://php.net/autoload "autoloader") classes from file paths. It is fully interoperable, and can be used in addition to any other autoloading specification, including [PSR-0](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-0.md "PSR-0"). This PSR also describes where to place files that will be autoloaded according to the specification.

## Basics

We star by adding some configuration to the _composer.json_ file

    {
        ...
        "autoload": {
            "psr-4": { "App\\": "src/" }
        },
        ...
    }

Now we create a _src_ directory and in there a couple of PHP class like: Calculator.php and Sum.php

_src/Calculator.php_

    <?php
    
    namespace App;
    
    class Calculator
    {
    
    }

_src/Operation/Sum.php_

    <?php
    
    namespace App\Operation;
    
    class Sum
    {
    
    }

It comes the moment to put in some logic:

_src/Calculator.php_

    <?php
    
    namespace App;
    
    use App\Operation\Sum;
    
    class Calculator
    {
        public function sum(int ...$operators): int
        {
            $operation = new Sum();
    
            return $operation->operate($operators);
        }
    }

_src/Operation/Sum.php_

    <?php
    
    namespace App\Operation;
    
    class Sum
    {
        /**
         * @param int ...$operands
         *
         * @return int
         */
        public function operate(int ...$operands): int
        {
            $result = 0;
    
            foreach ($operands as $operand) {
                $result += $operand;
            }
    
            return $result;
        }
    }

Focus your attention to the _namespace_ (line 3 in both files) and the _use_ (line 5 in _Calculator.php_). PSR-4 allow us to be clean and simple and still have all advantages of an autoloader.

Reached this point... [What the heck is a PSR](/post/php-3-what-the-hack-is-a-psr/ "What is a PSR")?